from unittest import TestCase
from unittest.mock import patch
from auth_token.security import AuthToken
from auth_token.utils import Payload


class TestAuthToken(TestCase):

    def test_encode(self):
        auth = AuthToken(
            sub='123456789',
            exp=3,
            secret_key='secret_key'
        )
        response = auth.encode()

        self.assertIsNotNone(response.token)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.message, 'ok')

    @patch('auth_token.security.AuthToken.decode')
    def test_decode_ok(self, mock_decode):
        # Mocks
        mock_decode.return_value = Payload(
            exp=12345,
            iat=1234,
            sub='12345678',
            uuid='1412-1234-1234'
        )
        auth = AuthToken(
            sub='123456789',
            exp=3,
            secret_key='secret_key'
        )
        payload = auth.decode('token')
        self.assertEqual(payload.exp, 12345)
        self.assertEqual(payload.iat, 1234)
        self.assertEqual(payload.sub, '12345678')
        self.assertEqual(payload.uuid, '1412-1234-1234')
